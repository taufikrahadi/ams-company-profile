import { supabase } from "@/app/api/supabase";
import PageBanner from "@/app/components/common/PageBanner";
import SectionTitle from "@/app/components/common/SectionTitle";
import { RootLayout } from "@/layouts/layout";
import Head from "next/head";
import React from "react";

export const getServerSideProps = async () => {
  const [app] = await Promise.all([
    supabase.from("app_settings").select("*").single(),
  ]);
  return { props: { app: app.data } };
};

function Contact({ app }: { app: any }) {
  return (
    <RootLayout
      email={app.email}
      logo={app.logo}
      whatsapp={app.whatsapp}
      topBar={{
        address: app.address,
        facebook: app.facebook,
        instagram: app.instagram,
        tiktok: app.tiktok,
      }}
    >
      <Head>
        <title>Anak Muda Sukses - Kontak Kami</title>
      </Head>

      <PageBanner
        headerImage="https://images.unsplash.com/photo-1544725121-be3bf52e2dc8?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2067&q=80"
        pageName="Contact"
        sectionName=""
        title="Contact"
      ></PageBanner>

      <section className="main-container pb-4">
        <div className="container">
          <div className="row justify-content-center">
            <SectionTitle
              title="Sampaikan Kebutuhan Anda"
              subTitle="Hubungi Kami"
            />
          </div>
          <div className="row">
            <div className="col-md-4">
              <div className="ts-service-box-bg text-center h-100">
                <span className="ts-service-icon icon-round">
                  <i className="fas fa-map-marker-alt mr-0"></i>
                </span>
                <div className="ts-service-box-content">
                  <h4>Kunjungi Kantor Kami</h4>
                  <p>{app.address}</p>
                </div>
              </div>
            </div>

            <div className="col-md-4">
              <div className="ts-service-box-bg text-center h-100">
                <span className="ts-service-icon icon-round">
                  <i className="fa fa-envelope mr-0"></i>
                </span>
                <div className="ts-service-box-content">
                  <h4>Email</h4>
                  <p>{app.email}</p>
                </div>
              </div>
            </div>

            <div className="col-md-4">
              <div className="ts-service-box-bg text-center h-100">
                <span className="ts-service-icon icon-round">
                  <i className="fa fa-phone-square mr-0"></i>
                </span>
                <div className="ts-service-box-content">
                  <h4>Hubungi Kami</h4>
                  <p>{app.whatsapp}</p>
                </div>
              </div>
            </div>

            <div className="col-md-12 mt-4 google-map">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6008.756724763904!2d110.34594298747305!3d-7.743743601005557!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a59a8df806579%3A0x9915395a4d9a85b7!2sKopi%20Djitoe!5e0!3m2!1sid!2sid!4v1696945116154!5m2!1sid!2sid"
                width="600"
                height="450"
                style={{ border: 0, width: "100%" }}
                loading="lazy"
              ></iframe>
            </div>
          </div>
        </div>
      </section>
    </RootLayout>
  );
}

export default Contact;
