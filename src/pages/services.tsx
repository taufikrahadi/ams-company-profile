import { supabase } from "@/app/api/supabase";
import PageBanner from "@/app/components/common/PageBanner";
import SectionTitle from "@/app/components/common/SectionTitle";
import ServiceItem from "@/app/components/service/ServiceItem";
import { RootLayout } from "@/layouts/layout";
import Head from "next/head";
import React from "react";

export const getServerSideProps = async () => {
  const [services, app] = await Promise.all([
    supabase.from("services").select("*").eq("is_active", true),
    supabase.from("app_settings").select("*").single(),
  ]);

  return {
    props: { services: services.data, app: app.data },
  };
};

function Services({ services, app }: { services: any; app: any }) {
  console.log(services, app);

  return (
    <RootLayout
      email={app.email}
      logo={app.logo}
      whatsapp={app.whatsapp}
      topBar={{
        address: app.address,
        facebook: app.facebook,
        instagram: app.instagram,
        tiktok: app.tiktok,
      }}
    >
      <PageBanner
        headerImage="https://images.unsplash.com/photo-1673305467649-42b89815a243?auto=format&fit=crop&q=80&w=2070&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
        pageName="Service"
        sectionName=""
        title="Service"
      />

      <Head>
        <title>Anak Muda Sukses - Layanan Kami</title>
      </Head>

      <section className="main-container pb-4">
        <div className="container">
          <div className="row justify-content-center">
            <SectionTitle title="" subTitle="Layanan Kami" />
          </div>

          <div className="row justify-content-center">
            {services.map((service: any) => (
              <div key={service.id} className="col-lg-4 col-md-12 my-5">
                <ServiceItem
                  banner={service.banner}
                  description={service.description}
                  title={service.name}
                />
              </div>
            ))}
          </div>
        </div>
      </section>
    </RootLayout>
  );
}

export default Services;
