import { supabase } from "@/app/api/supabase";
import ArticleItem from "@/app/components/articles/ArticleItem";
import Sidebar from "@/app/components/articles/Sidebar";
import PageBanner from "@/app/components/common/PageBanner";
import { RootLayout } from "@/layouts/layout";
import Head from "next/head";
import React, { useEffect } from "react";

export const getServerSideProps = async () => {
  const [articles, app] = await Promise.all([
    supabase.from('articles').select('*').eq('is_active', true).order('created_at', { ascending: false }),
    supabase.from("app_settings").select("*").single(),
  ])

  return {
    props: {
      articles: articles.data, app: app.data
    }
  }
}

function Index({ articles, app }: { articles: any[], app: any }) {

  useEffect(() => {
    console.log(articles)
  }, [])


  return <RootLayout
    email={app.email}
    logo={app.logo}
    whatsapp={app.whatsapp}
    topBar={{
      address: app.address,
      facebook: app.facebook,
      instagram: app.instagram,
      tiktok: app.tiktok,
    }}
  >
    <Head>
      <title>Anak Muda Sukses - Kontak Kami</title>
    </Head>

    <PageBanner
      headerImage="https://images.unsplash.com/photo-1544725121-be3bf52e2dc8?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2067&q=80"
      pageName="Articles"
      sectionName=""
      title="Articles"
    ></PageBanner>

    <section className="main-container pb-4">
      <div className="container">
        <div className="row">

          {/* Left Sidebar | Widget Recent Post */}
          <div className="col-lg-4 order-1 order-lg-0">
            <Sidebar fromTable="articles" position="left" />
          </div>
          {/* End Of Sidebar */}

          {/* Posts */}
          <div className="col-lg-8 mb-5 mb-lg-0 order-0 order-lg-1">
            {
              articles.map((article) => {
                return <ArticleItem created_at={article.created_at} description={article.description} key={article.id} id={article.id} title={article.title} banner={article.banner} body={article.body} posted_at={article.created_at} type="articles"></ArticleItem>
              })
            }
          </div>
        </div>
      </div>
    </section>
  </RootLayout>

}

export default Index;
