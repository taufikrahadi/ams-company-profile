import { supabase } from "@/app/api/supabase";
import PageBanner from "@/app/components/common/PageBanner";
import Categories from "@/app/components/projects/Categories";
import { useProjects } from "@/app/services/projects";
import { RootLayout } from "@/layouts/layout";
import Head from "next/head";
import Link from "next/link";
import React, { useCallback, useEffect, useState } from "react";

export const getServerSideProps = async () => {
  const [projectCategories, app, photoProject] = await Promise.all([
    supabase.from("project_categories").select("*").eq("is_active", true),
    supabase.from("app_settings").select("*").single(),
    supabase
      .from("projects")
      .select("*, project_photos(photo), project_categories(id, name)")
      .eq("is_active", true)
      .order("created_at", { ascending: false }),
  ]);

  return {
    props: {
      projectCategories: projectCategories.data,
      app,
      projects: photoProject.data,
    },
  };
};
function index(props: any) {
  const { projectCategories, app, projects } = props;

  const [items, setItems] = useState(projects);
  const [selectedFilter, setSelectedFilter] = useState("all");

  const filterItems = (cat: any) => {
    const newItems = projects.filter(
      (newval: any) => newval.project_id?.category_id.name === cat
    );

    setSelectedFilter(cat);
    setItems(newItems);
  };

  const fetchProjects = useCallback(
    (id: any) => {
      useProjects(undefined, id as any).then((data) => setItems(data as any));
    },
    [supabase]
  );

  useEffect(() => {
    fetchProjects(selectedFilter);
  }, [selectedFilter]);

  return (
    <RootLayout
      topBar={{
        address: app.data.address,
        facebook: app.data.facebook,
        instagram: app.data.instagram,
        tiktok: app.data.tiktok,
      }}
      email={app.data.email}
      logo={app.data.logo}
      whatsapp={app.data.whatsapp}
    >
      <Head>
        <title>Anak Muda Sukses - Project</title>
      </Head>

      <PageBanner
        title="Project"
        headerImage="https://plus.unsplash.com/premium_photo-1682142040426-fa6f2856e57f?auto=format&fit=crop&q=80&w=2071&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
        pageName="Project"
        sectionName="project"
      ></PageBanner>

      <section className="main-section pb-4">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <Categories
                data={projectCategories}
                defaultFilter={(id: string) => {
                  setSelectedFilter(id);
                }}
                filterItems={(id: string) => fetchProjects(id)}
                useShowAll={true}
                items={projectCategories}
                selectedFilter={selectedFilter}
                setItems={setItems}
              />
              <div className="row shuffle-wrapper">
                {items.map((data: any, i: any) => {
                  return (
                    <div key={i} className="col-lg-4 col-md-6 shuffle-item">
                      <div className="project-img-container">
                        <a
                          className="gallery-popup"
                          href="images/projects/project1.jpg"
                          aria-label="project-img"
                        >
                          <img
                            className="img-fluid"
                            src={
                              data.project_photos.length > 0
                                ? data.project_photos[0].photo
                                : "https://placehold.co/380x304.png"
                            }
                            alt="project-img"
                            width={380}
                            height={304}
                          />
                        </a>
                        <div className="project-item-info">
                          <div className="project-item-info-content">
                            <h3 className="project-item-title">
                              <Link href={`/projects/${data.id}`}>
                                {data?.title}
                              </Link>
                            </h3>
                            <p className="project-cat">
                              {data.project_categories.name}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
              <br />
            </div>
          </div>
        </div>
      </section>
    </RootLayout>
  );
}

export default index;
