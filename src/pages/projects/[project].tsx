import { supabase } from "@/app/api/supabase";
import PageBanner from "@/app/components/common/PageBanner";
import { RootLayout } from "@/layouts/layout";
import Head from "next/head";
import { useRouter } from "next/router";
import Slider from "react-slick";

export const getServerSideProps = async (searchParams: any) => {
  const { project } = searchParams.query;
  const [projectCategories, app, photoProject] = await Promise.all([
    supabase.from("project_categories").select("*").eq("is_active", true),
    supabase.from("app_settings").select("*").single(),
    supabase
      .from("projects")
      .select("*, project_photos(photo), project_categories(id, name)")
      .eq("id", project)
      .single(),
  ]);

  return {
    props: {
      projectCategories: projectCategories.data,
      app,
      project: photoProject,
    },
  };
};

const detailProject = (props: any) => {
  const { query } = useRouter();
  const { app } = props;

  const { data: project } = props.project;

  return (
    <RootLayout
      topBar={{
        address: app.data.address,
        facebook: app.data.facebook,
        instagram: app.data.instagram,
        tiktok: app.data.tiktok,
      }}
      email={app.data.email}
      logo={app.data.logo}
      whatsapp={app.data.whatsapp}
    >
      <Head>
        <title>Anak Muda Sukses - {project.title}</title>
      </Head>

      <PageBanner
        pageName="project"
        sectionName=""
        title={project.title}
        headerImage={
          project.project_photos.length > 0
            ? project.project_photos[0].photo
            : undefined
        }
      />

      <div className="container mt-5">
        <div className="row">
          <div className="col-lg-8">
            <div className="page-slider small-bg">
              <Slider
                className="banner-carousel banner-carousel-1 mb-0"
                autoplay={true}
                speed={1000}
                dots={false}
                arrows={true}
                slidesToShow={1}
                slidesToScroll={1}
              >
                {project.project_photos.map((project_photo: any) => {
                  return (
                    <div className="item">
                      <img
                        loading="lazy"
                        className="img-fluid"
                        src={project_photo.photo}
                        alt="project-image"
                      />
                    </div>
                  );
                })}
              </Slider>
            </div>
          </div>

          <div className="col-lg-4 mt-5 mt-lg-0">
            <h3 className="column-title mrt-0">{project.title}</h3>
            <p>{project.description}</p>

            <ul className="project-info list-unstyled">
              <li>
                <p className="project-info-label">Lokasi</p>
                <p className="project-info-content">{project.location}</p>
              </li>

              <li>
                <p className="project-info-label">Selesai Pada</p>
                <p className="project-info-content">{project.date_completed}</p>
              </li>

              <li>
                <p className="project-info-label">Kategori</p>
                <p className="project-info-content">
                  {project.project_categories.name}
                </p>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </RootLayout>
  );
};

export default detailProject;
