import { supabase } from "../app/api/supabase";
import Carousel from "../app/components/carousel/Carousel";
import Facts from "../app/components/common/Facts";
import Footer from "../app/components/common/Footer";
import Header from "../app/components/common/Header";
import SectionTitle from "../app/components/common/SectionTitle";
import TopBar from "../app/components/common/TopBar";
import Head from "next/head";
import { RootLayout } from "@/layouts/layout";
import Categories from "@/app/components/projects/Categories";
import Collapse from "@/app/components/common/Collapse";
import { useCallback, useEffect, useState } from "react";
import HomepageItem from "@/app/components/service/HomepageItem";
import TestimonialSection from "@/app/components/testimonial/TestimonialSection";
import ClientItem from "@/app/components/client/ClientItem";
import { useServices } from "@/app/services/services";
import { useTestimonials } from "@/app/services/testimonials";
import Categories2 from "@/app/components/projects/Categories2";
import { useProjects } from "@/app/services/projects";
import Link from "next/link";

export const getServerSideProps = async () => {
  const [projectCategories, projects, app, clients, testimonials] =
    await Promise.all([
      supabase.from("project_categories").select("*").eq("is_active", true),
      supabase
        .from("projects")
        .select("*, project_photos(photo), project_categories(id, name)")
        .eq("is_active", true),
      supabase.from("app_settings").select("*").single(),
      supabase.from("clients").select("*").eq("is_active", true),
      supabase.from("testimonials").select("*").eq("is_active", true),
    ]);

  return { props: { projectCategories, app, clients, testimonials, projects } };
};

export default function Home(props: any) {
  const { projectCategories, app, projects: sProjects } = props;
  const [collapseState, setCollapseState] = useState({
    safety: true,
    customerService: false,
    integrity: false,
  });

  const collapseData = [
    {
      code: "safety",
      title: "safety",
      description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    },
    {
      code: "customerService",
      title: "customer service",
      description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    },
    {
      code: "integrity",
      title: "integrity",
      description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    },
  ];

  const [services, setServices] = useState<any[]>([]);
  const [testimonials, setTestimonials] = useState<any[]>([]);
  const [selectedProjectCategory, setSelectedProjectCategory] = useState<
    string | number | null
  >("all");

  const [projects, setProjects] = useState(sProjects.data);

  useEffect(() => {
    useServices().then((data) => setServices(data));
    useTestimonials().then((data) => setTestimonials(data));
    useProjects(6, "all").then((data) => setProjects(data));
  }, []);

  const fetchProjects = useCallback(
    (id: any) => {
      useProjects(6, id as any).then((data) => setProjects(data as any));
    },
    [supabase]
  );

  useEffect(() => {
    fetchProjects(selectedProjectCategory);
  }, [selectedProjectCategory]);

  return (
    <RootLayout
      topBar={{
        address: app.data.address,
        facebook: app.data.facebook,
        instagram: app.data.instagram,
        tiktok: app.data.tiktok,
      }}
      email={app.data.email}
      logo={app.data.logo}
      whatsapp={app.data.whatsapp}
    >
      <Head>
        <title>Anak Muda Sukses</title>
        <meta name="description" content={app.data.description} />
      </Head>

      <div className="body-inner">
        <Carousel />

        <section className="call-to-action-box no-padding">
          <div className="container">
            <div className="action-style-box">
              <div className="row align-items-center">
                <div className="col-md-8 text-center text-md-left">
                  <div className="call-to-action-text">
                    <h3 className="action-title">
                      Kami Paham Akan Kebutuhan Anda
                    </h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section id="ts-features" className="ts-features">
          <div className="container">
            <div className="row">
              <div className="col-lg-6">
                <div className="ts-intro">
                  <h2 className="into-title">Tentang Kami</h2>
                  <h3 className="into-sub-title">
                    mengerjakan permintaan dengan sepenuh hati.
                  </h3>
                  <p>
                    We are rethoric question ran over her cheek When she reached
                    the first hills of the Italic Mountains, she had a last view
                    back on the skyline of her hometown Bookmarksgrove, the
                    headline of Alphabet Village and the subline of her own
                    road, the Line Lane.
                  </p>
                </div>

                <div className="gap-20"></div>

                <div className="row">
                  <div className="col-md-6">
                    <div className="ts-service-box">
                      <span className="ts-service-icon">
                        <i className="fas fa-trophy"></i>
                      </span>
                      <div className="ts-service-box-content">
                        <h3 className="service-box-title">Reputasi Sempurna</h3>
                      </div>
                    </div>
                  </div>

                  <div className="col-md-6">
                    <div className="ts-service-box">
                      <span className="ts-service-icon">
                        <i className="fas fa-sliders-h"></i>
                      </span>
                      <div className="ts-service-box-content">
                        <h3 className="service-box-title">
                          Membangun Kepercayaan
                        </h3>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-6">
                    <div className="ts-service-box">
                      <span className="ts-service-icon">
                        <i className="fas fa-thumbs-up"></i>
                      </span>
                      <div className="ts-service-box-content">
                        <h3 className="service-box-title">Komitmen Penuh</h3>
                      </div>
                    </div>
                  </div>

                  <div className="col-md-6">
                    <div className="ts-service-box">
                      <span className="ts-service-icon">
                        <i className="fas fa-users"></i>
                      </span>
                      <div className="ts-service-box-content">
                        <h3 className="service-box-title">
                          Team Yang Profesional
                        </h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-lg-6 mt-4 mt-lg-0">
                <h3 className="into-sub-title">Keunggulan Kami</h3>
                <p>
                  Minim Austin 3 wolf moon scenester aesthetic, umami odio
                  pariatur bitters. Pop-up occaecat taxidermy street art,
                  tattooed beard literally.
                </p>

                <div
                  className="accordion accordion-group"
                  id="our-values-accordion"
                >
                  {collapseData.map((collapse) => (
                    <Collapse
                      title={collapse.title}
                      description={collapse.description}
                      isOpen={(collapseState as any)[collapse.code]}
                      unShow={() =>
                        setCollapseState({
                          ...collapseState,
                          [collapse.code]: !(collapseState as any)[
                            collapse.code
                          ],
                        })
                      }
                      key={collapse.code}
                    />
                  ))}
                </div>
              </div>
            </div>
          </div>
        </section>

        <section id="facts" className="facts-area dark-bg">
          <div className="container">
            <div className="facts-wrapper">
              <div className="row">
                <div className="col-md-3 col-sm-6 ts-facts mt-5 mt-sm-0">
                  <Facts
                    title="Total Project"
                    count={99}
                    logo="https://jqvqzffraxvpzquaqiev.supabase.co/storage/v1/object/public/company-profile/project-management.png?t=2023-09-24T14%3A49%3A58.288Z"
                  />
                </div>

                <div className="col-md-3 col-sm-6 ts-facts mt-5 mt-sm-0">
                  <Facts
                    title="Staff Members"
                    count={44}
                    logo="https://jqvqzffraxvpzquaqiev.supabase.co/storage/v1/object/public/company-profile/management.png"
                  />
                </div>

                <div className="col-md-3 col-sm-6 ts-facts mt-5 mt-md-0">
                  <Facts
                    title="Hours Of Work"
                    count={40}
                    logo="https://jqvqzffraxvpzquaqiev.supabase.co/storage/v1/object/public/company-profile/time-management.png?t=2023-09-24T14%3A52%3A50.718Z"
                  />
                </div>

                <div className="col-md-3 col-sm-6 ts-facts mt-5 mt-md-0">
                  <Facts
                    title="Region Experience"
                    count={10}
                    logo="https://jqvqzffraxvpzquaqiev.supabase.co/storage/v1/object/public/company-profile/world.png?t=2023-09-24T14%3A54%3A28.006Z"
                  />
                </div>
              </div>
            </div>
          </div>
        </section>

        <section id="ts-service-area" className="ts-service-area pb-0">
          <div className="container">
            <div className="row text-center">
              <div className="col-12">
                <h2 className="section-title">We Are Specialists In</h2>
                <h3 className="section-sub-title">What We Do</h3>
              </div>
            </div>

            <div className="container">
              <div className="row">
                {services.map((service: any) => (
                  <div className="col-lg-4 col-sm-6" key={service.id}>
                    <HomepageItem
                      title={service.name}
                      description={service.description}
                      icon={service.icon}
                    />
                  </div>
                ))}
              </div>
            </div>
          </div>
        </section>

        <section id="project-area" className="project-area solid-bg">
          <div className="container">
            <SectionTitle
              title="Work Of Excellence"
              subTitle="Recent Projects"
            />

            <div className="row">
              <div className="col-12">
                <Categories
                  data={projectCategories.data}
                  defaultFilter={(id: string) => {
                    console.log(id);
                    setSelectedProjectCategory(id);
                  }}
                  filterItems={(id: string) => fetchProjects(id)}
                  useShowAll={true}
                  items={projectCategories.data}
                  selectedFilter={selectedProjectCategory}
                  setItems={setProjects}
                />
                <div className="row shuffle-wrapper">
                  {projects.map((data: any, i: any) => {
                    return (
                      <div key={i} className="col-lg-4 col-md-6 shuffle-item">
                        <div className="project-img-container">
                          <a
                            className="gallery-popup"
                            href="images/projects/project1.jpg"
                            aria-label="project-img"
                          >
                            <img
                              className="img-fluid"
                              src={
                                data.project_photos.length > 0
                                  ? data.project_photos[0].photo
                                  : "https://placehold.co/380x304.png"
                              }
                              alt="project-img"
                              width={380}
                              height={304}
                            />
                          </a>
                          <div className="project-item-info">
                            <div className="project-item-info-content">
                              <h3 className="project-item-title">
                                <Link href={`/projects/${data.id}`}>
                                  {data?.title}
                                </Link>
                              </h3>
                              <p className="project-cat">
                                {data.project_categories.name}
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>

              <div className="col-12">
                <div className="general-btn text-center">
                  <Link className="btn btn-primary" href="/projects">
                    View All Projects
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className="content">
          <div className="container">
            <div className="row">
              <div className="col-lg-6">
                <TestimonialSection testimonials={testimonials} />
              </div>

              <div className="col-lg-6 mt-5 mt-lg-0">
                <h3 className="column-title">Happy Clients</h3>

                <div className="row all-clients">
                  {props.clients.data.map((client: any) => (
                    <div className="col-sm-4 col-6" key={client.id}>
                      <ClientItem logo={client.logo} name={client.name} />
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </RootLayout>
  );
}
