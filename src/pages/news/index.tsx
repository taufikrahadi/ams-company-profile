import { supabase } from "@/app/api/supabase";
import ArticleItem from "@/app/components/articles/ArticleItem";
import Sidebar from "@/app/components/articles/Sidebar";
import PageBanner from "@/app/components/common/PageBanner";
import { RootLayout } from "@/layouts/layout";
import Head from "next/head";
import React, { useEffect } from "react";

export const getServerSideProps = async () => {
  const [news, app] = await Promise.all([
    supabase.from('news').select('*').eq('is_active', true).order('created_at', { ascending: false }),
    supabase.from("app_settings").select("*").single(),
  ])

  return {
    props: {
      news: news.data, app: app.data
    }
  }
}

function Index({ news, app }: { news: any[], app: any }) {
  return <RootLayout
    email={app.email}
    logo={app.logo}
    whatsapp={app.whatsapp}
    topBar={{
      address: app.address,
      facebook: app.facebook,
      instagram: app.instagram,
      tiktok: app.tiktok,
    }}
  >
    <Head>
      <title>Anak Muda Sukses - Berita</title>
    </Head>

    <PageBanner
      headerImage="https://images.unsplash.com/photo-1544725121-be3bf52e2dc8?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2067&q=80"
      pageName="News"
      sectionName=""
      title="News"
    ></PageBanner>

    <section className="main-container pb-4">
      <div className="container">
        <div className="row">

          {/* Left Sidebar | Widget Recent Post */}
          <div className="col-lg-4 order-1 order-lg-0">
            <Sidebar fromTable="news" position="left" />
          </div>
          {/* End Of Sidebar */}

          {/* Posts */}
          <div className="col-lg-8 mb-5 mb-lg-0 order-0 order-lg-1">
            {
              news.map((_news) => {
                return <ArticleItem created_at={_news.created_at} key={_news.id} id={_news.id} title={_news.title} banner={_news.banner} body={_news.body} description={_news.description} posted_at={_news.created_at} type="news"></ArticleItem>
              })
            }
          </div>
        </div>
      </div>
    </section>
  </RootLayout>

}

export default Index;
