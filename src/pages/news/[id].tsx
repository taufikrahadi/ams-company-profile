import { supabase } from '@/app/api/supabase'
import Sidebar from '@/app/components/articles/Sidebar'
import PageBanner from '@/app/components/common/PageBanner'
import { RootLayout } from '@/layouts/layout'
import Head from 'next/head'
import React, { useEffect, useState } from 'react'
import SingleArticle from '@/app/components/articles/SingleArticle'

export async function getServerSideProps(searchParams: any) {
    const { id } = searchParams.query
    const { data } = await supabase.from('news').select('*').eq('id', id).single()
    const { data: app } = await supabase.from('app_settings').select('*').single()
    return { props: { data, app } }
}

function DetailArticle(props: any) {

    const { app, data } = props

    return (
        <RootLayout
            email={app.email}
            logo={app.logo}
            whatsapp={app.whatsapp}
            topBar={{
                address: app.address,
                facebook: app.facebook,
                instagram: app.instagram,
                tiktok: app.tiktok,
            }}
        >
            <Head>
                <title>{data.title}</title>
            </Head>

            <PageBanner
                headerImage="https://images.unsplash.com/photo-1544725121-be3bf52e2dc8?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2067&q=80"
                pageName='article'
                sectionName=""
                title={data.title}
            />

            <section className="main-container pb-4">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4">
                            <Sidebar fromTable='news' position='left' />
                        </div>

                        <div className="col-lg-8">
                            <SingleArticle data={{ ...data, type: 'News' }} />
                        </div>
                    </div>
                </div>
            </section>
        </RootLayout>
    )
}

export default DetailArticle