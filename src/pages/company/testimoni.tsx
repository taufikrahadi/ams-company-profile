import { supabase } from "@/app/api/supabase";
import TestimonialSection from "@/app/components/testimonial/TestimonialSection";
import { RootLayout } from "@/layouts/layout";
import Head from "next/head";
import React from "react";

export const getServerSideProps = async () => {
  const [app, testimonials] = await Promise.all([
    supabase.from("app_settings").select("*").single(),
    supabase.from("testimonials").select("*").eq("is_active", true),
  ]);
  return { props: { app: app.data, testimonials: testimonials } };
};

function Testimoni({ app, testimonials }: { app: any; testimonials: any }) {
  return (
    <RootLayout
      topBar={{
        address: app.address,
        facebook: app.facebook,
        instagram: app.instagram,
        tiktok: app.tiktok,
      }}
      email={app.email}
      logo={app.logo}
      whatsapp={app.whatsapp}
    >
      <Head>
        <title>Anak Muda Sukses - Testimoni</title>
      </Head>

      <section className="main-container pb-4">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-12">
              <TestimonialSection testimonials={testimonials.data} />
            </div>
          </div>
        </div>
      </section>
    </RootLayout>
  );
}

export default Testimoni;
