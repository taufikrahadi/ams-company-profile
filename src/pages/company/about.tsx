import { supabase } from "@/app/api/supabase";
import { RootLayout } from "@/layouts/layout";
import Head from "next/head";
import React from "react";

export const getServerSideProps = async () => {
  const [app] = await Promise.all([
    supabase.from("app_settings").select("*").single(),
  ]);

  return { props: { app: app.data } };
};

function about({ app }: { app: any }) {
  return (
    <RootLayout
      topBar={{
        address: app.address,
        facebook: app.facebook,
        instagram: app.instagram,
        tiktok: app.tiktok,
      }}
      email={app.email}
      logo={app.logo}
      whatsapp={app.whatsapp}
    >
      <Head>
        <title>Anak Muda Sukses - Tentang Kami</title>
      </Head>

      <div
        id="banner-area"
        className="banner-area"
        style={{
          backgroundImage:
            "url(https://images.unsplash.com/photo-1632862378103-8248dccb7e3d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1965&q=80)",
          backgroundPosition: "center center",
        }}
      >
        <div className="banner-text">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="banner-heading">
                  <h1 className="banner-title">About</h1>
                  <nav aria-label="breadcrumb">
                    <ol className="breadcrumb justify-content-center">
                      <li className="breadcrumb-item">
                        <a href="#">company</a>
                      </li>
                      <li
                        className="breadcrumb-item active"
                        aria-current="page"
                      >
                        About Us
                      </li>
                    </ol>
                  </nav>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <section id="main-container" className="main-container">
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <h3 className="column-title">Who We Are</h3>
              <p>
                when Gregor Samsa woke from troubled dreams, he found himself
                transformed in his bed into a horrible vermin.
              </p>
              <blockquote>
                <p>
                  Semporibus autem quibusdam et aut officiis debitis aut rerum
                  est aut optio cumque nihil necessitatibus autemn ec tincidunt
                  nunc posuere ut
                </p>
              </blockquote>
              <p>
                He lay on his armour-like back, and if he lifted. ultrices
                ultrices sapien, nec tincidunt nunc posuere ut. Lorem ipsum
                dolor sit amet, consectetur adipiscing elit. If you are going to
                use a passage of Lorem Ipsum, you need to be sure there isn’t
                anything embarrassing.
              </p>
            </div>

            <div className="col-lg-6 mt-5 mt-lg-0">
              {/* <div id="page-slider" className="page-slider small-bg">
                <div
                  className="item"
                  style="background-image:url(images/slider-pages/slide-page1.jpg)"
                >
                  <div className="container">
                    <div className="box-slider-content">
                      <div className="box-slider-text">
                        <h2 className="box-slide-title">Leadership</h2>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  className="item"
                  style="background-image:url(images/slider-pages/slide-page2.jpg)"
                >
                  <div className="container">
                    <div className="box-slider-content">
                      <div className="box-slider-text">
                        <h2 className="box-slide-title">Relationships</h2>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  className="item"
                  style="background-image:url(images/slider-pages/slide-page3.jpg)"
                >
                  <div className="container">
                    <div className="box-slider-content">
                      <div className="box-slider-text">
                        <h2 className="box-slide-title">Performance</h2>
                      </div>
                    </div>
                  </div>
                </div>
              </div> */}
              <img
                style={{ width: "100%" }}
                src="https://images.unsplash.com/photo-1516216628859-9bccecab13ca?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2069&q=80"
              />
            </div>
          </div>
        </div>
      </section>
    </RootLayout>
  );
}

export default about;
