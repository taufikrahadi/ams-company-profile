import { supabase } from "@/app/api/supabase";
import PageBanner from "@/app/components/common/PageBanner";
import PersonTeam from "@/app/components/team/PersonTeam";
import { RootLayout } from "@/layouts/layout";
import Head from "next/head";
import React from "react";

export const getServerSideProps = async () => {
  const [app, teams] = await Promise.all([
    supabase.from("app_settings").select("*").single(),
    supabase
      .from("teams")
      .select("*")
      .eq("is_active", true)
      .order("role", { ascending: true }),
  ]);

  return {
    props: { app: app.data, teams: teams.data },
  };
};

function Team({ app, teams }: { app: any; teams: any }) {
  return (
    <RootLayout
      topBar={{
        address: app.address,
        facebook: app.facebook,
        instagram: app.instagram,
        tiktok: app.tiktok,
      }}
      email={app.email}
      logo={app.logo}
      whatsapp={app.whatsapp}
    >
      <Head>
        <title>Anak Muda Sukses - Tim Kami</title>
      </Head>

      <PageBanner
        headerImage=""
        pageName="Tim Kami"
        sectionName="company"
        title="Tim Kami"
      />

      <section className="main-container pb-4">
        <div className="container">
          <div className="row justify-content-center">
            {[...teams].map((team: any) => (
              <div className="col-lg-3 col-sm-6 mb-5" key={team.id}>
                <PersonTeam
                  name={team.fullname}
                  id={team.id}
                  description={team.description}
                  photo={team.photo}
                  role={team.role}
                  facebook={team.facebook}
                  instagram={team.instagram}
                  linkedin={team.linkedin}
                  twitter={team.twitter}
                />
              </div>
            ))}
          </div>
        </div>
      </section>
    </RootLayout>
  );
}

export default Team;
