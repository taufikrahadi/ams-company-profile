import React from "react";

export interface TestimonialItemProps {
  body: string;
  photo: string;
  person_name: string;
  occupation: string;
}

function TestimonialItem(props: TestimonialItemProps) {
  return (
    <div className="item">
      <div className="quote-item">
        <span className="quote-text">{props.body}</span>

        <div className="quote-item-footer">
          <img
            loading="lazy"
            className="testimonial-thumb"
            src={props.photo}
            alt="testimonial"
          />
          <div className="quote-item-info">
            <h3 className="quote-author">{props.person_name}</h3>
            <span className="quote-subtext">{props.occupation}</span>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TestimonialItem;
