import React from "react";
import TestimonialItem, { TestimonialItemProps } from "./TestimonialItem";
import Slider from "react-slick";

export interface TestimonialSectionProps {
  testimonials: TestimonialItemProps[];
}

function TestimonialSection(props: TestimonialSectionProps) {
  return (
    <>
      <h3 className="column-title">Testimonials</h3>

      <div id="testimonial-slide" className="testimonial-slide">
        <Slider
          arrows={false}
          dots={true}
          slidesToShow={1}
          autoplay={true}
          autoplaySpeed={3000}
        >
          {props.testimonials.map((testimonial) => (
            <TestimonialItem {...testimonial} key={testimonial.occupation} />
          ))}
        </Slider>
      </div>
    </>
  );
}

export default TestimonialSection;
