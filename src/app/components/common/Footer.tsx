import React from "react";

function Footer() {
  return (
    <footer id="footer" className="footer bg-overlay">
      <div className="footer-main">
        <div className="container">
          <div className="row justify-content-between">
            <div className="col-lg-4 col-md-6 footer-widget footer-about">
              <h3 className="widget-title">About Us</h3>
              <img
                loading="lazy"
                className="footer-logo"
                src="https://images.placeholders.dev"
                alt="Constra"
              />
              <div className="footer-social">
                <ul>
                  <li>
                    <a
                      href="https://facebook.com/themefisher"
                      aria-label="Facebook"
                    >
                      <i className="fab fa-facebook-f"></i>
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://twitter.com/themefisher"
                      aria-label="Twitter"
                    >
                      <i className="fab fa-twitter"></i>
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://instagram.com/themefisher"
                      aria-label="Instagram"
                    >
                      <i className="fab fa-instagram"></i>
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://github.com/themefisher"
                      aria-label="Github"
                    >
                      <i className="fab fa-github"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 footer-widget mt-5 mt-md-0">
              <h3 className="widget-title">Working Hours</h3>
              <div className="working-hours">
                We work 7 days a week, every day excluding major holidays.
                Contact us if you have an emergency.
                <br />
                <br /> Monday - Sunday:{" "}
                <span className="text-right">08:00 - 18:00 </span>
              </div>
            </div>

          </div>
        </div>
      </div>

      <div className="copyright">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-md-12">
              <div className="copyright-info text-center">
                <span>
                  Copyright &copy; {new Date().getFullYear()} Developed by{" "}
                  <a
                    target="_blank"
                    href="https://www.linkedin.com/in/taufik-rahadi-b6493a10b/"
                  >
                    Taufik Rahadi
                  </a>{" "}
                  &amp; AMS Construction
                </span>
              </div>
            </div>
          </div>

          <div
            id="back-to-top"
            data-spy="affix"
            data-offset-top="10"
            className="back-to-top position-fixed"
          >
            <button className="btn btn-primary" title="Back to Top">
              <i className="fa fa-angle-double-up"></i>
            </button>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
