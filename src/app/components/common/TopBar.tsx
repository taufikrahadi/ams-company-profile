import React from "react";

export interface TopBarProps {
  address: string;
  facebook: string;
  instagram: string;
  tiktok: string;
}

function TopBar(props: TopBarProps) {
  return (
    <div id="top-bar" className="top-bar">
      <div className="container">
        <div className="row">
          <div className="col-lg-8 col-md-8">
            <ul className="top-info text-center text-md-left">
              <li>
                <i className="fas fa-map-marker-alt"></i>{" "}
                <p className="info-text">{props.address}</p>
              </li>
            </ul>
          </div>

          <div className="col-lg-4 col-md-4 top-social text-center text-md-right">
            <ul className="list-unstyled">
              <li>
                <a title="Facebook" href={props.facebook}>
                  <span className="social-icon">
                    <i className="fab fa-facebook-f"></i>
                  </span>
                </a>
                <a title="Tiktok" href={props.tiktok}>
                  <span className="social-icon">
                    <i className="fa-brands fa-tiktok"></i>
                  </span>
                </a>
                <a title="Instagram" href={props.instagram}>
                  <span className="social-icon">
                    <i className="fab fa-instagram"></i>
                  </span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TopBar;
