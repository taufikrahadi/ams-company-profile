import React from "react";

export class PageBannerProps {
  headerImage?: string;
  title: string;
  sectionName: string;
  pageName: string;
}

function PageBanner(props: PageBannerProps) {
  return (
    <div
      id="banner-area"
      className="banner-area"
      style={{
        backgroundImage: `url(${
          props.headerImage ??
          "https://images.unsplash.com/photo-1632862378103-8248dccb7e3d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1965&q=80"
        })`,
        backgroundPosition: "center bottom",
      }}
    >
      <div className="banner-text">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="banner-heading">
                <h1 className="banner-title">{props.title}</h1>
                <nav aria-label="breadcrumb">
                  <ol className="breadcrumb justify-content-center">
                    <li className="breadcrumb-item">
                      <a href="#">{props.sectionName}</a>
                    </li>
                    <li className="breadcrumb-item active" aria-current="page">
                      {props.pageName}
                    </li>
                  </ol>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PageBanner;
