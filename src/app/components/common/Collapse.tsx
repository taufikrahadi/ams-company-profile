import React from "react";

export interface CollapseProps {
  title: string;
  description: string;
  isOpen: boolean;
  unShow(): void;
}

function Collapse({ title, description, isOpen, unShow }: CollapseProps) {
  return (
    <div className="card">
      <div className="card-header p-0 bg-transparent" id="headingOne">
        <h2 className="mb-0">
          <button
            className="btn btn-block text-left"
            type="button"
            onClick={(e) => unShow()}
          >
            {title}
          </button>
        </h2>
      </div>

      <div
        className={`collapse ${isOpen ? "show" : undefined}`}
        aria-labelledby="headingOne"
        data-parent="#our-values-accordion"
      >
        <div className="card-body">{description}</div>
      </div>
    </div>
  );
}

export default Collapse;
