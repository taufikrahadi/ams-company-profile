import React from "react";

export interface ContactUsBarProps {
  whatsapp: string;
  email: string;
  logo: string;
}

function ContactUsBar(props: ContactUsBarProps) {
  return (
    <header id="header" className="header-one">
      <div className="bg-white">
        <div className="container">
          <div className="logo-area">
            <div className="row align-items-center">
              <div className="logo col-lg-3 text-center text-lg-left mb-3 mb-md-5 mb-lg-0">
                <a className="d-block" href="index.html">
                  <img loading="lazy" src={props.logo} alt="Constra" />
                </a>
              </div>

              <div className="col-lg-9 header-right text-right">
                <ul className="top-info-box">
                  <li>
                    <div className="info-box">
                      <div className="info-box-content">
                        <p className="info-box-title">Hubungi Kami</p>
                        <p className="info-box-subtitle">{props.whatsapp}</p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div className="info-box">
                      <div className="info-box-content">
                        <p className="info-box-title">Email</p>
                        <p className="info-box-subtitle">{props.email}</p>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
}

export default ContactUsBar;
