import Link from "next/link";
import { useEffect, useState } from "react";
import $ from "jquery";

function Navbar() {
  const toggleClick = (e: any) => {
    console.log("clicked");

    $("site-navigation .dropdown-toggle").siblings(".dropdown-menu").animate(
      {
        height: "toggle",
      },
      300
    );
  };

  const [showNav, setShowNav] = useState(false);
  const [showCompany, setShowCompany] = useState(false);

  useEffect(() => {
    if (($(window).width() as any) < 992) {
      $(".site-navigation .dropdown-toggle").on("click", function () {
        $(this).siblings(".dropdown-menu").animate(
          {
            height: "toggle",
          },
          300
        );
      });

      const navbarHeight = $(".site-navigation").outerHeight();
      $(".site-navigation .navbar-collapse").css(
        "max-height",
        "calc(100vh - " + navbarHeight + "px)"
      );
    }
  }, []);

  return (
    <div className="site-navigation">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <nav className="navbar navbar-expand-lg navbar-dark p-0">
              <button
                onClick={() => setShowNav(!showNav)}
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target=".navbar-collapse"
                aria-controls="navbar-collapse"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>

              <div
                id="navbar-collapse"
                className={`collapse navbar-collapse ${showNav ? "show" : ""}`}
              >
                <ul className="nav navbar-nav mr-auto">
                  <li className="nav-item">
                    <Link href="/" className="nav-link">
                      Home
                    </Link>
                  </li>

                  <li className={`nav-item dropdown ${showCompany ? "show" : ''}`}>
                    <a
                      href="#"
                      className={`nav-link dropdown-toggle`}
                      onClick={() => {
                        setShowCompany(true)
                      }}
                    >
                      Company <i className="fa fa-angle-down"></i>
                    </a>
                    <ul className="dropdown-menu" role="menu">
                      <li>
                        <Link href="/company/about">About Us</Link>
                      </li>
                      <li>
                        <Link href="/company/team">Team Kami</Link>
                      </li>
                      <li>
                        <Link href="/company/testimoni">Testimoni</Link>
                      </li>
                    </ul>
                  </li>

                  <li className="nav-item dropdown">
                    <Link href="/projects" className="nav-link">
                      Projects
                    </Link>
                  </li>

                  <li className="nav-item dropdown">
                    <Link href="/services" className="nav-link">
                      Services
                    </Link>
                  </li>

                  <li className="nav-item dropdown">
                    <Link href="/articles" className="nav-link">
                      Articles
                    </Link>
                  </li>

                  <li className="nav-item dropdown">
                    <Link href="/news" className="nav-link">
                      News
                    </Link>
                  </li>

                  <li className="nav-item">
                    <Link className="nav-link" href="/contact">
                      Contact
                    </Link>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </div>

      <script
        src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossOrigin="anonymous"
      ></script>
      <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct"
        crossOrigin="anonymous"
      ></script>
    </div>
  );
}

export default Navbar;
