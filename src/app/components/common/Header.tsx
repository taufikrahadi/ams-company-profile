import React from "react";
import ContactUsBar from "./ContactUsBar";
import Navbar from "./Navbar";

function Header(props: { app: Record<string, any> }) {
  return (
    <header id="header" className="header-one">
      <ContactUsBar
        email={props.app.email}
        whatsapp={props.app.whatsapp}
        logo={props.app.logo}
      />

      <Navbar />
    </header>
  );
}

export default Header;
