import React from "react";

export interface CategoryItemProps {
  id: string;
  name: string;
  filterItems: any;
  selectedFilter: any;
  setSelectedFilter: any;
}

function CategoryItem(props: CategoryItemProps) {
  return (
    <label className={props.selectedFilter === props.id ? 'active' : ''} htmlFor={props.name.toLowerCase()} onClick={props.filterItems}>
      <input
        type="radio"
        name="shuffle-filter"
        id={props.name.toLowerCase()}
        value={props.name.toLowerCase()}
        onClick={props.setSelectedFilter}
      />
      {props.name}
    </label>
  );
}

export default CategoryItem;
