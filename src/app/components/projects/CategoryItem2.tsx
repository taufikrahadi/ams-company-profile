import React from 'react'

function CategoryItem2(props: { is_active: boolean, name: string, setSelectedFilter: any, id: string | number | null }) {
    return (
        <label
            className={props.is_active ? 'active' : ''}
            htmlFor={props.name.toLowerCase()}
            onClick={props.setSelectedFilter(props.id)}
        >
            <input
                type="radio"
                name="shuffle-filter"
                id={props.name.toLowerCase()}
                value={props.name.toLowerCase()}
            />
            {props.name}
        </label>
    )
}

export default CategoryItem2