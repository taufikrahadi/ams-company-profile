import React, { useState } from "react";
import CategoryItem, { CategoryItemProps } from "./CategoryItem";

export interface CategoriesProps {
  useShowAll: boolean;
  items: CategoryItemProps[];
  filterItems: any;
  setItems: any;
  data: any;
  selectedFilter: any;
  defaultFilter: any;
}

function Categories(props: CategoriesProps) {
  return (
    <div className="shuffle-btn-group">
      <label className={props.selectedFilter === 'all' ? "active" : ""} htmlFor="all">
        <input
          type="radio"
          name="shuffle-filter"
          id="all"
          value="all"
          defaultChecked
          onClick={() => { props.defaultFilter('all') }}
        />
        Show All
      </label>
      {props.items.map((item) => (
        <CategoryItem id={item.id} setSelectedFilter={() => { console.log(item.id); props.defaultFilter(item.id) }} name={item.name} filterItems={() => props.filterItems(item.id)} key={item.name} selectedFilter={props.selectedFilter} />
      ))}
    </div>
  );
}

export default Categories;
