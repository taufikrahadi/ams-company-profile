import React from 'react'
import CategoryItem from './CategoryItem'
import CategoryItem2 from './CategoryItem2'

function Categories2(props: { selectedFilter: string | number | null, items: any[], setSelectedFilter: any }) {
    console.log(props)
    return (
        <div className="shuffle-btn-group">
            <label className={props.selectedFilter === 'all' ? "active" : ""} htmlFor="all">
                <input
                    type="radio"
                    name="shuffle-filter"
                    id="all"
                    value="all"
                    defaultChecked
                    onClick={() => { console.log('anjing'); props.setSelectedFilter('all') }}
                />
                Show All
            </label>
            {props.items.map((item) => (
                <CategoryItem2
                    key={item.id}
                    is_active={props.selectedFilter === item.id}
                    name={item.name}
                    id={item.id}
                    setSelectedFilter={props.setSelectedFilter}
                />
            ))}
        </div>
    )
}

export default Categories2