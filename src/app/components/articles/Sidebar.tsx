import { supabase } from '@/app/api/supabase'
import Link from 'next/link'
import React, { useCallback, useEffect, useState } from 'react'

function Sidebar(props: { fromTable: string, position?: string }) {
    const [recentPosts, setRecentPosts] = useState<any[]>([])

    const { fromTable, position } = props
    const fetchData = useCallback(() => {
        return supabase.from(fromTable).select('*').eq('is_active', true).limit(3).order('created_at', { ascending: false }).then(({ data }) => {
            setRecentPosts(data as any)
        })
    }, [])

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className={`sidebar sidebar-${position ? position : 'left'}`}>
            <div className="widget recent-posts">
                <h3 className="widget-title">Recent Posts</h3>
                <ul className="list-unstyled">
                    {
                        recentPosts.map((recentPost) => (
                            <li className="d-flex align-items-center" key={recentPost.id}>
                                <div className="posts-thumb">
                                    <Link href={`/${fromTable}/${recentPost.id}`}>
                                        <img loading="lazy" alt="img" src={recentPost.banner} />
                                    </Link>
                                </div>
                                <div className="post-info">
                                    <h4 className="entry-title">
                                        <Link href={`/${fromTable}/${recentPost.id}`}>{recentPost.title}</Link>
                                    </h4>
                                </div>
                            </li>
                        ))
                    }
                </ul>
            </div>
        </div>
    )
}

export default Sidebar