import React, { useEffect, useState } from 'react'
import dayjs from 'dayjs'
import ReactHtmlParser from 'react-html-parser'

function SingleArticle({ data }: { data: Record<string, any> }) {
    const [articleUrl, setArticleUrl] = useState('')

    const handleShare = (socialMedia: 'facebook' | 'twitter') => {
        const shareUrl = getShareUrl(socialMedia);
        window.open(shareUrl, '_blank');
    };

    const getShareUrl = (socialMedia: 'facebook' | 'twitter') => {
        switch (socialMedia) {
            case 'facebook':
                return `https://www.facebook.com/sharer/sharer.php?u=${encodeURIComponent(articleUrl)}`;
            case 'twitter':
                return `https://twitter.com/intent/tweet?url=${encodeURIComponent(articleUrl)}&text=${encodeURIComponent(data.title)}`;
            // Add more cases for other social media platforms
            default:
                return;
        }
    };

    useEffect(() => {
        setArticleUrl(window.location.href)
    }, [])

    return (
        <div className="post-content post-single">
            <div className="post-media post-image">
                <img loading="lazy" src={data.banner} className="img-fluid" alt="post-image" />
            </div>

            <div className="post-body">
                <div className="entry-header">
                    <div className="post-meta">
                        <span className="post-cat">
                            <i className="far fa-folder-open"></i><a href={`/${data.type.toLowerCase()}`}> {data.type}</a>
                        </span>
                        <span className="post-meta-date"><i className="far fa-calendar"></i>{dayjs(data.created_at).format('DD MMM YYYY')}</span>
                    </div>
                    <h2 className="entry-title">
                        {data.title}
                    </h2>
                </div>

                <div className="entry-content">
                    {ReactHtmlParser(data.body)}
                </div>

                <div className="tags-area d-flex align-items-center justify-content-between">

                    <div className="share-items">
                        <ul className="post-social-icons list-unstyled">
                            <li className="social-icons-head">Share:</li>
                            <li><a onClick={e => handleShare('facebook')} href="#"><i className="fab fa-facebook-f"></i></a></li>
                            <li><a onClick={e => handleShare('twitter')} href="#"><i className="fab fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    )
}

export default SingleArticle