import dayjs from 'dayjs'
import Link from 'next/link'
import React from 'react'


export interface ArticleItemProps {
    id: string
    title: string
    banner: string
    body: string
    type: string
    posted_at: string
    description: string
    created_at: string
}

function ArticleItem(props: ArticleItemProps) {
    return (
        <div className="post">
            <div className="post-media post-image">
                <img loading="lazy" src={props.banner} className="img-fluid" alt="post-image" />
            </div>

            <div className="post-body">
                <div className="entry-header">
                    <div className="post-meta">
                        <span className="post-cat">
                            <i className="far fa-folder-open"></i><a href="#">{props.type}</a>
                        </span>
                        <span className="post-meta-date"><i className="far fa-calendar"></i> {dayjs(props.created_at).format('DD MMM YYYY')}</span>
                    </div>
                    <h2 className="entry-title">
                        <Link href={`/${props.type}/${props.id}`}>{props.title}</Link>
                    </h2>
                </div>

                <div className="entry-content">
                    <div>{props.description}...</div>
                </div>

                <div className="post-footer">
                    <Link href={`/${props.type}/${props.id}`} className="btn btn-primary">Continue Reading</Link>
                </div>

            </div>
        </div>
    )
}

export default ArticleItem