import React from "react";

export interface ClientItemProps {
  name: string;
  logo: string;
}

function ClientItem(props: ClientItemProps) {
  return (
    <figure className="clients-logo">
      <a href="#!">
        <img
          loading="lazy"
          className="img-fluid"
          src={props.logo}
          alt={props.name}
        />
      </a>
    </figure>
  );
}

export default ClientItem;
