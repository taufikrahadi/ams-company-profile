import React from "react";

export interface CarouselItemProps {
  bgImage: string;
  title: string;
  subTitle: string;
  linkAction: string;
}

function CarouselItem(props: CarouselItemProps) {
  return (
    <>
      <div
        className="banner-carousel-item"
        style={{ backgroundImage: `url(${props.bgImage})` }}
      >
        <div className="slider-content">
          <div className="container h-100">
            <div className="row align-items-center h-100">
              <div className="col-md-12 text-center">
                <h2 className="slide-title">{props.title}</h2>
                <h3 className="slide-sub-title">{props.subTitle}</h3>
                <p>
                  <a href={props.linkAction} className="slider btn btn-primary">
                    Lihat Lebih Lanjut
                  </a>
                  <a
                    href="contact.html"
                    className="slider btn btn-primary border"
                  >
                    Hubungi Sekarang
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default CarouselItem;
