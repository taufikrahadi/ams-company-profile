import React from "react";
import { isNotEmpty } from "class-validator";

export class PersonTeamProps {
  id: string;
  photo: string;
  name: string;
  role: string;
  description: string;
  facebook?: string;
  instagram?: string;
  twitter?: string;
  linkedin?: string;
}

function PersonTeam(props: PersonTeamProps) {
  return (
    <div className="ts-team-wrapper">
      <div className="team img-wrapper">
        <img
          src={props.photo}
          alt={`${props.name} photo`}
          height={255}
          width={290}
        />
      </div>

      <div className="ts-team-content-classic">
        <h3 className="ts-name">{props.name}</h3>
        <p className="ts-designation">{props.role}</p>
        <p className="ts-description">{props.description}</p>
        <div className="team-social-icons">
          {isNotEmpty(props.facebook) ? (
            <a target="_blank" href={props.facebook}>
              <i className="fab fa-facebook-f"></i>
            </a>
          ) : (
            <></>
          )}

          {isNotEmpty(props.instagram) ? (
            <a href={props.instagram} target="_blank" rel="noopener noreferrer">
              <i className="fab fa-instagram"></i>
            </a>
          ) : (
            <></>
          )}
          {isNotEmpty(props.twitter) ? (
            <a target="_blank" href={props.twitter}>
              <i className="fab fa-twitter"></i>
            </a>
          ) : (
            <></>
          )}
          {isNotEmpty(props.linkedin) ? (
            <a target="_blank" href={props.linkedin}>
              <i className="fab fa-linkedin"></i>
            </a>
          ) : (
            <></>
          )}
        </div>
      </div>
    </div>
  );
}

export default PersonTeam;
