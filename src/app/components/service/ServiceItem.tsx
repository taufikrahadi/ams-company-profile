import React from "react";

export interface ServiceItemProps {
  title: string;
  description: string;
  banner: string;
}

function ServiceItem({ title, banner, description }: ServiceItemProps) {
  return (
    <div className="ts-service-box">
      <div className="ts-service-image-wrapper">
        <img
          loading="lazy"
          width={350}
          height={250}
          src={banner}
          alt="service-image"
        />
      </div>
      <div className="d-flex">
        <div className="ts-service-info">
          <h3 className="service-box-title">
            <a href="service-single.html">{title}</a>
          </h3>
          <p>{description}</p>
          {/* <a
            className="learn-more d-inline-block"
            href=""
            aria-label="service-details"
          >
            <i className="fa fa-caret-right"></i> Learn more
          </a> */}
        </div>
      </div>
    </div>
  );
}

export default ServiceItem;
