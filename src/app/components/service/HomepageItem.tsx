import React from "react";

export interface ServiceHomepagItemProps {
  title: string;
  description: string;
  icon: string;
}

function HomepageItem(props: ServiceHomepagItemProps) {
  return (
    <div className="row">
      <div className="col-3">
        <img loading="lazy" height={60} src={props.icon} alt="service-icon" />
      </div>

      <div className="col-9">
        <h3 className="">{props.title}</h3>
        <p>{props.description}</p>
      </div>
    </div>
  );
}

export default HomepageItem;
