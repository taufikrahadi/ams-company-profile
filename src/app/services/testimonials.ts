import { supabase } from "../api/supabase";

export const useTestimonials = async () => {
  const { data } = await supabase
    .from("testimonials")
    .select("*")
    .eq("is_active", true);

  return data ?? [];
};
