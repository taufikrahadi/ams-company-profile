import { supabase } from "../api/supabase";

export const useServices = async () => {
  const { data } = await supabase
    .from("services")
    .select("*")
    .order("name", { ascending: true })
    .eq("is_active", true);

  return data ?? [];
};
