import { supabase } from "../api/supabase";

export const useProjects = async (
  limit?: number,
  projectCategoryId?: string
) => {
  const query = supabase
    .from("projects")
    .select("*, project_photos(photo), project_categories(id, name)")
    .eq("is_active", true)
    .order("created_at", { ascending: false });

  console.log(projectCategoryId);

  if (limit) query.limit(limit);
  if (projectCategoryId && projectCategoryId !== "all")
    query.eq("category_id", projectCategoryId);

  const { data } = await query;

  return data ?? [];
};
