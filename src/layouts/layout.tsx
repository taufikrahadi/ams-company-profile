import "bootstrap/dist/css/bootstrap.css";
import "../plugins/fontawesome/css/all.min.css";
import "../css/style.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { PropsWithChildren } from "react";
import TopBar, { TopBarProps } from "@/app/components/common/TopBar";
import Header from "@/app/components/common/Header";
import Carousel from "@/app/components/carousel/Carousel";
import Footer from "@/app/components/common/Footer";

export interface RootLayoutProps extends PropsWithChildren {
  topBar: TopBarProps;
  email: string;
  whatsapp: string;
  logo: string;
}

export const RootLayout = ({
  email,
  whatsapp,
  logo,
  children,
  topBar,
}: RootLayoutProps) => {
  return (
    <div className="body-inner">
      <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
      ></link>

      <TopBar {...topBar} />

      <Header app={{ email, whatsapp, logo }} />

      {children}

      <Footer />

      <script
        src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossOrigin="anonymous"
      ></script>
      <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct"
        crossOrigin="anonymous"
      ></script>
    </div>
  );
};
